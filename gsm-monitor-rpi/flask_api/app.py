#!/usr/bin/env python3

import time
import serial
import sqlite3
import json

from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)
ser = serial.Serial('/dev/serial0')
ser.isOpen()

@app.route('/')
def hello():
    return 'hello flask'


@app.route('/check-status')
def check_AT():
    ser.write(('AT\r').encode())
    out = ('').encode('utf-8')
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)
    if out != '':
        print(out.decode('utf-8'))
        return 'status AT: ' + out.decode('utf-8')


@app.route('/check-cellist')
def check_CELLIST():
    ser.write(('AT+CELLIST\r').encode())
    out = ('').encode('utf-8')
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)
    if out != '':
        print(out.decode('utf-8'))
        return 'CELLIST: ' + out.decode('utf-8')

@app.route('/command', methods=['GET'])
def check_any_AT():
    command = request.args.get('cmd')
    ser.write((command+'\r').encode())
    out = ('').encode('utf-8')
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)
    if out != '':
        print(out.decode('utf-8'))
        return 'command: ' + out.decode('utf-8')


@app.route('/getAllCellsFromDB', methods=['GET'])
def getCellsFromDB():
    conn = sqlite3.connect('/home/pi/gsm/main.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM CELLIST ORDER BY timestamp DESC''')
    #c.execute('''SELECT json_object('MNC', MNC, 'timestamp', timestamp) AS json_result FROM (SELECT * FROM CELLIST ORDER BY timestamp);''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getCellListCount', methods=['GET'])
def getCellListCount():
    conn = sqlite3.connect('/home/pi/gsm/main.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT COUNT(id) FROM CELLIST''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getCarrierFromDB', methods=['GET'])
def getCarrierFromDB():
    mcc = request.args.get('mcc')
    mnc = request.args.get('mnc')
    conn = sqlite3.connect('/home/pi/gsm/carriers.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM CARRIERS WHERE mcc = (?) AND mnc = (?)''', (mcc, mnc))
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getAllCarriersFromDB', methods=['GET'])
def getAllCarriersFromDB():
    conn = sqlite3.connect('/home/pi/gsm/carriers.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM CARRIERS''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getAllCarriersCount', methods=['GET'])
def getAllCarriersCount():
    conn = sqlite3.connect('/home/pi/gsm/carriers.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT COUNT(*) FROM CARRIERS''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getCellTowersWithLocation', methods=['GET'])
def getCellTowersWithLocation():
    conn = sqlite3.connect('/home/pi/gsm/cell_towers.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM poland LIMIT 100''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getStats', methods=['GET'])
def getStats():
    conn = sqlite3.connect('/home/pi/gsm/stats_config.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM STATS''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getConfig', methods=['GET'])
def getConfig():
    conn = sqlite3.connect('/home/pi/gsm/stats_config.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM CONFIG''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/getOperatorsStats', methods=['GET'])
def getOperatorsStats():
    conn = sqlite3.connect('/home/pi/gsm/main.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT network, count(network) FROM CELLIST GROUP BY network''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)

@app.route('/updateConfig', methods=['GET'])
def updateConfig():
    lat = request.args.get('lat')
    lon = request.args.get('lon')
    alert_email_active = request.args.get('alert_email_active')
    alert_email_rcp = request.args.get('alert_email_rcp')
    alert_sms_active = request.args.get('alert_sms_active')
    alert_sms_rcp = request.args.get('alert_sms_rcp')
    location_descr = request.args.get('location_descr')
    print(lat, lon, alert_email_active, alert_email_rcp, alert_sms_active, alert_sms_rcp, location_descr)
    conn = sqlite3.connect('/home/pi/gsm/stats_config.db')
    c = conn.cursor()
    c.execute('''UPDATE CONFIG SET device_lat=(?), device_lon=(?), alert_recipient_mail=(?), alert_recipient_sms=(?), alert_mail_active=(?), alert_sms_active=(?), location_descr=(?) WHERE device_name="gsm-monitor-1"''', (lat, lon, alert_email_rcp, alert_sms_rcp, alert_email_active, alert_sms_active, location_descr))
    conn.commit()
    result = {"status" : "ok"}
    return jsonify(result)

@app.route('/getAllNotifications', methods=['GET'])
def getAllNotifications():
    conn = sqlite3.connect('/home/pi/gsm/notifications.db')
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute('''SELECT * FROM EVENTS ORDER BY timestamp DESC''')
    result = [dict(row) for row in c.fetchall()]
    return jsonify(result)


@app.after_request
def add_header(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    #response.headers['Access-Control-Allow-Methods'] = 'GET,POST,OPTIONS'
    #response.headers['Access-Control-Allow-Headers'] = '*'
    return response
