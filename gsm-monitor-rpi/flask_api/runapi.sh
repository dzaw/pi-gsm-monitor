#!/bin/bash
echo "* starting flask api"
cd /home/pi/flask/
export FLASK_ENV=development
flask run --host=0.0.0.0 &
echo "* flask api started"
