#!/usr/bin/env python3

import time
import serial
import sqlite3
import os.path
import codecs
import smtplib, ssl

ser = serial.Serial('/dev/serial0')
ser.isOpen()


# mail settings
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
port = 465  # For SSL
smtp_server = "smtp.gmail.com"
sender_email = "dev@gmail.com"  # Enter your address
receiver_email = "dag@gmail.com"  # Enter receiver address


def writeStartTime():
    ts = int(time.time())
    if not os.path.isfile('/home/pi/gsm/main.db'):
        conn = sqlite3.connect('/home/pi/gsm/stats_config.db')
        c = conn.cursor()
        c.execute('''INSERT OR IGNORE INTO STATS VALUES (?,?,?)''', (ts,0,"gsm-monitor-1"))
        conn.commit()
        c.close()


def createDB():
    conn = sqlite3.connect('/home/pi/gsm/main.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS CELLIST
             ([id] text PRIMARY KEY,[mcc] text, [mnc] text, [arfcn] text, [rxlev] text, [cellid_hex] text, [cellid_dec] text, [lac] text, [bsic] text, [timestamp] integer, [country] text, [country_code] text, [iso] text, [network] text, [lat] text, [lon] text)''')
    conn.commit()
    c.close()

def sendMail(subject, message, recipient):
    print("Send mail to " + recipient)
    context = ssl.create_default_context()
    msg = MIMEMultipart('related')
    msg['Subject'] = subject
    msg['From'] = sender_email
    msg['To'] = recipient
    html = message
    part2 = MIMEText(html, 'html')
    msg.attach(part2)
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, "")
        server.sendmail(sender_email, recipient, msg.as_string())
    print("mail sent..")


def checkCellList():
    ser.write(('AT+CELLIST=1\r').encode())
    time.sleep(3)
    ser.write(('AT+CELLIST\r').encode())
    out = ('').encode('utf-8')
    time.sleep(2)
    while ser.inWaiting() > 0:
        out += ser.read(1)
    if out != '':
        #print(out.decode('utf-8'))
        #print('CELLIST: ' + out.decode('utf-8'))
        ret = out.decode('utf-8')
        print(ret.splitlines())

        timestamp = int(time.time())
        print(timestamp)

        conn = sqlite3.connect('/home/pi/gsm/main.db')
        conn.execute('''ATTACH DATABASE '/home/pi/gsm/carriers.db' AS carriers''')
        conn.execute('''ATTACH DATABASE '/home/pi/gsm/cell_towers.db' AS towers''')
        conn.execute('''ATTACH DATABASE '/home/pi/gsm/notifications.db' AS notifications''')
        conn.execute('''ATTACH DATABASE '/home/pi/gsm/stats_config.db' AS config''')
        conn.row_factory = sqlite3.Row
        c = conn.cursor()

        for i in ret.splitlines():
            if i.startswith("+CELLIST: "):
                cell = i[10:].split(",")
                print(cell)
                mcc = cell[0]
                mnc = cell[1]
                cellid_hex = cell[4]
                cellid_dec = "{}".format(int('0x'+cellid_hex, 0))
                lac_area_hex = cell[5]
                lac_area_dec = "{}".format(int('0x'+lac_area_hex, 0))
                lac_area_hex_changed_endian = codecs.encode(codecs.decode(lac_area_hex, 'hex')[::-1], 'hex').decode()
                lac_area_dec2 = "{}".format(int('0x'+lac_area_hex_changed_endian, 0))
                #print(cellid_hex, cellid_dec)
                #print(lac_area_hex_changed_endian, lac_area_dec2)
                c.execute('''SELECT * FROM carriers.CARRIERS WHERE mcc = (?) AND mnc = (?)''', (mcc, mnc))
                carrier = [dict(row) for row in c.fetchall()]
                country = carrier[0]['country']
                country_code = carrier[0]['country_code']
                iso = carrier[0]['iso']
                network = carrier[0]['network']
                #c.execute('''SELECT * FROM towers.poland WHERE area=(?) AND mcc=(?) AND net=(?) ORDER BY updated DESC LIMIT 1''', (lac_area_dec, mcc, mnc))
                c.execute('''SELECT * FROM towers.poland WHERE area=(?) AND cell=(?) ORDER BY updated DESC LIMIT 1''', (lac_area_dec2, cellid_dec))
                tower = [dict(rowt) for rowt in c.fetchall()]
                print(len(tower))
                if not len(tower) > 0:
                     c.execute('''SELECT * FROM towers.poland WHERE area=(?) AND mcc=(?) AND net=(?) ORDER BY updated DESC LIMIT 1''', (lac_area_dec, mcc, mnc))
                     tower = [dict(rowt) for rowt in c.fetchall()]
                #print(len(tower))
                lat = tower[0]['lat']
                lon = tower[0]['lon']
                #print(lat, lon)
                #TODO test if empty in carrier db
                #print(country, country_code, iso, network)
                id = "" + cell[1]+cell[4]+cell[5]+cell[6]
                print("-- generated cell ID: " + id)
                c.execute("SELECT COUNT(*) FROM CELLIST WHERE id='" + id + "'")
                record_check = [dict(rowc) for rowc in c.fetchall()]
                exists = record_check[0]['COUNT(*)']
                if exists > 0:
                    print("-- skip -- record exists in db")
                else:
                    print("-- insert and notify")
                    c.execute('''INSERT OR IGNORE INTO CELLIST VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)''', (id,cell[0],cell[1],cell[2],cell[3],cell[4],cellid_dec,cell[5],cell[6],timestamp,country,country_code,iso,network,lat,lon))
                    subject = "[GSM-monitor] Nowa stacja GSM w okolicy"
                    message = "Wykryto nową stację GSM w okolicy urządzenia:\nMCC " + str(cell[0]) + "\nMNC " + str(cell[1]) + "\nCellID " + str(cell[4]) + "\nLAC " + str(cell[5]) + "\nOperator " + network + "\ntimestamp " + str(timestamp)
                    c.execute('''INSERT INTO notifications.EVENTS (title, message, timestamp) VALUES (?,?,?)''', (subject, message, timestamp))
                    c.execute('''SELECT alert_mail_active, alert_recipient_mail FROM config.CONFIG''')
                    check_config = [dict(rowcc) for rowcc in c.fetchall()]
                    send_mail = check_config[0]['alert_mail_active']
                    alert_rcp_mail = check_config[0]['alert_recipient_mail']
                    if send_mail == 1:
                        sendMail(subject, message, alert_rcp_mail)
        conn.commit()

print("==============")

writeStartTime()
createDB()
checkCellList()

print("==============")
