#!/usr/bin/env python3

import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
#ser = serial.Serial(
#    port='/dev/serial0',
#    baudrate=9600,
#    parity=serial.PARITY_ODD,
#    stopbits=serial.STOPBITS_TWO,
#    bytesize=serial.SEVENBITS
#)

ser = serial.Serial('/dev/serial0')

ser.isOpen()

print('Enter your commands below.\r\nInsert "exit" to leave the application.')

user_input=1
while 1 :
    # get keyboard input
    # Python2
    #input = raw_input(">> ")
    # Python 3 users
    user_input = input(">> ")
    if user_input == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
        ser.write((user_input + '\r').encode())
        out = ('').encode('utf-8')
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
            out += ser.read(1)

        if out != '':
            print(out.decode('utf-8'))
